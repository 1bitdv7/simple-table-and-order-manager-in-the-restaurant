const express = require('express')
const OrderController = require('../controller/OrderController/OrderController')
const ProdutoController = require('../controller/ProdutoController/ProdutoController')
const RenderViewsController = require('../controller/RenderViewsController/RenderViewsController')
const UserController = require('../controller/UserController/UserController')

const AuthMiddleware = require('../middleware/auth/auth')
const uploadFile  = require('../middleware/upload/upload')

const Routes = express.Router()

const Router = (app)=>{
    
    Routes.get('/',RenderViewsController.index)
    Routes.get('/home',RenderViewsController.home)

    
    Routes.get('/history',AuthMiddleware.Sesion,RenderViewsController.orderlist)
    Routes.get('/orders',AuthMiddleware.Sesion,RenderViewsController.ordersAtive)
    Routes.get('/order/:id/add',AuthMiddleware.Sesion,RenderViewsController.indexAdd)
    Routes.get('/order/:id/fine',AuthMiddleware.Sesion,RenderViewsController.OrderFine)
    Routes.get('/order/:id/preview',AuthMiddleware.Sesion,RenderViewsController.preview)

    Routes.delete('/deleteorder/:id',OrderController.DeleteOrder)

    Routes.get('/configuracao',AuthMiddleware.Sesion,RenderViewsController.configuracao)

    Routes.get('/produto/:id/editar',AuthMiddleware.Sesion,RenderViewsController.EditarProdutoView)
    Routes.get('/produtos',AuthMiddleware.Sesion,RenderViewsController.produtosList)
    Routes.get('/produto/add',AuthMiddleware.Sesion,RenderViewsController.addprodutosGet)
    
    Routes.post('/produto/search/',AuthMiddleware.isAuth,ProdutoController.searchProdutos)
    
    Routes.post('/produtos/add',AuthMiddleware.isAuth,ProdutoController.addprodutos)
    Routes.post('/produtos/update',AuthMiddleware.isAuth,ProdutoController.Updateprodutos)
    Routes.post('/detalhes/',OrderController.detalhesOrder)
    Routes.post('/order/:orderid/:id',OrderController.addOrderProdutos)
    Routes.delete('/produto/:id',ProdutoController.deleteProduto)
    Routes.post('/order',AuthMiddleware.isAuth,OrderController.order)
    Routes.post('/orderdetalhe',AuthMiddleware.isAuth,ProdutoController.quantityRemoveOrproduto)



    Routes.get('/relatorio',AuthMiddleware.Sesion,RenderViewsController.relatorio)
    Routes.get('/relatorios/fechodiario/:id',AuthMiddleware.Sesion,RenderViewsController.fechodiario)
    Routes.get('/relatorio/vendas',AuthMiddleware.Sesion,RenderViewsController.vendas)
    Routes.get('/relatorio/vendas/venda',AuthMiddleware.Sesion,RenderViewsController.IndexVendas)
    Routes.get('/relatorio/funcionario/relatorio',AuthMiddleware.Sesion,RenderViewsController.FunctionarioRelatorio)
    Routes.get('/relatorio/funcionarios/index',AuthMiddleware.Sesion,RenderViewsController.FunctionarioIndex)
    
    

    Routes.get('/usuarios',AuthMiddleware.Sesion,RenderViewsController.usuarios)
    Routes.get('/usuario/add',AuthMiddleware.Sesion,RenderViewsController.usuarioAdicionar)

    Routes.get('/usuario/add/:id',AuthMiddleware.Sesion,RenderViewsController.usuarioEditar)

    Routes.get('/logout',RenderViewsController.logout)


    Routes.post('/configuracao',uploadFile.single('file'),UserController.configuracao)
    Routes.get('/usuario/:id',AuthMiddleware.Sesion,RenderViewsController.usuarioAdicionar)
    Routes.post('/login',UserController.login)
    Routes.post('/register',uploadFile.single('file'),UserController.createUser)
    Routes.post('/updateuser',UserController.UpdateUser)
    app.use(Routes)
}


module.exports = Router