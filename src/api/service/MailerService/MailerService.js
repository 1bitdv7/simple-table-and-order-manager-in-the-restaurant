const nodemailer = require('nodemailer')
const MailerConfig = require('../../config/MailerConfig.js')
module.exports = nodemailer.createTransport(MailerConfig)