const {DataTypes, Model} = require("sequelize");

class Produtos extends Model {
    static init(sequelize) {
        super.init({
            produto_name: DataTypes.STRING,
            produto_price:DataTypes.FLOAT,
            produto_qtd: DataTypes.INTEGER,
            produto_descrisao:DataTypes.STRING
        }, {sequelize})
    }
    static association (model){
        this.belongsTo(model.Users,{foreignKey:'user_id' , as:'users'})
        this.belongsTo(model.Categorias,{foreignKey:'id_categoria' , as:'categorias'})
    }
}

module.exports = Produtos
