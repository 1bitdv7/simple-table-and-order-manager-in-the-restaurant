const {DataTypes, Model} = require("sequelize");

class Configuracao extends Model {
    static init(sequelize) {
        super.init({
            name:DataTypes.STRING,
            endereco:DataTypes.STRING,
            numero:DataTypes.STRING,
            email:DataTypes.STRING,
            nif:DataTypes.STRING,
            file:DataTypes.STRING,
            mensagem:DataTypes.STRING,
            desconto:DataTypes.FLOAT
        }, {sequelize})
    }
}

module.exports = Configuracao
