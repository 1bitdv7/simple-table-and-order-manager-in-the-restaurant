const {Model, DataTypes} = require("sequelize");

class Order extends Model {
    static init(sequelize) {
        super.init({
            nome: DataTypes.STRING,
            email: DataTypes.STRING,
            status: DataTypes.STRING,
            nif: DataTypes.STRING,
            data:DataTypes.DATEONLY,
            created_at: DataTypes.DATE
        }, {sequelize})
    }
    static association(model) {
        this.belongsTo(model.Users, {
            foreignKey: 'user_id',
            as: 'users'
        })
    }
}


module.exports = Order
