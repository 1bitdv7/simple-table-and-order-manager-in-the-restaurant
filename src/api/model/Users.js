const {Model, DataTypes} = require("sequelize");

class Users extends Model {
    static init(sequelize) {
        super.init({
            nome: DataTypes.STRING,
            email: DataTypes.STRING,
            password: DataTypes.STRING
        }, {sequelize})
    }
    static association(model) {
        this.belongsTo(model.Regras, {
            foreignKey: 'rules_id',
            as: 'regras'
        })
        this.belongsTo(model.Fotos, {
            foreignKey: 'foto_id',
            as: 'fotos'
        })
    }
}
module.exports = Users
