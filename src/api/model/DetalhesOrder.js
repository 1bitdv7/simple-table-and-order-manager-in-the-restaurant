const {Model, DataTypes} = require("sequelize");

class DetalhesOrder extends Model {
    static init(sequelize) {
        super.init({
            produto_qtd: DataTypes.INTEGER,
            total_order_price: DataTypes.FLOAT,
            data: DataTypes.DATEONLY
        }, {sequelize})
    }
    static association(model) {
        this.belongsTo(model.Produtos, {
            foreignKey: 'id_produto',
            as: 'produtos'
        })
        this.belongsTo(model.Order, {
            foreignKey: 'id_order',
            as: 'orders'
        })
    }
}


module.exports = DetalhesOrder
