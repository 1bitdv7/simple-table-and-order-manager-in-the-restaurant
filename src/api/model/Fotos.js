const {Model, DataTypes} = require("sequelize");

class Fotos extends Model {
    static init(sequelize) {
        super.init({
            foto: DataTypes.STRING
        }, {sequelize})
    }
}


module.exports = Fotos