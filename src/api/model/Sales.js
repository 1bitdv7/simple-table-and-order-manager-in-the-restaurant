const {Model, DataTypes} = require("sequelize");


class Sales extends Model {
    static init(sequelize) {
        super.init({
            produto_price_total: DataTypes.FLOAT,
            data: DataTypes.DATEONLY,
            created_at: DataTypes.DATE,
        }, {sequelize})
    }
    static association(models) {
        this.belongsTo(models.Users, {
            foreignKey: 'id_user',
            as: 'users'
        })
        this.belongsTo(models.Order, {
            foreignKey: 'id_order',
            as: 'orders'
        })
    }
}


module.exports = Sales
