 function handlerError(err,request,response,next){
    const errorStatusCode =  response.statusCode ? response.statusCode : 500
    response.status(errorStatusCode)
    response.json({
        message: err.message,
        stack: process.env.NODE_ENV == 'production' ? null : ''
    })
}

 function paginationDate(data,page,limit){
    const {count: totalItems,rows:line} = data;
    const currentPage = page ? +page : 1;
    const TotalPage = Math.ceil(totalItems/limit);
    return {totalItems,line,TotalPage,currentPage}
}

function levelSystemModule(number){
    return (number ==  1 ? "Garsonet" : (number == 2 ? "Atendente" : "Gerente"))
}
 function pagination(page,dinamic){
    var totalItem = dinamic;
    var pages = (page == 1) ? 0 : page - 1;
    pages = totalItem * pages;
    return {pages,totalItem}
}

module.exports = {pagination,handlerError,paginationDate,levelSystemModule}