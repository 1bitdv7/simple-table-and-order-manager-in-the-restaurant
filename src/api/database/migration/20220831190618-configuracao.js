'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('configuracaos', { 
      id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      name:{
        type: Sequelize.STRING,
        allowNull: false
      },
      endereco:{
        type: Sequelize.STRING,
        allowNull: false
      },
      numero:{
        type: Sequelize.STRING,
        allowNull: false
      },
      email:{
        type: Sequelize.STRING,
        allowNull: false
      },
      nif:{
        type: Sequelize.STRING,
        allowNull: false
      },
      file:{
        type: Sequelize.STRING,
        allowNull: false
      },
      mensagem:{
        type: Sequelize.STRING,
        allowNull: false
      },
      desconto:{
        type: Sequelize.FLOAT,
        allowNull: false
      },
    created_at: {
        type: Sequelize.DATE,
        allowNull: false
    },
    updated_at: {
        type: Sequelize.DATE,
        allowNull: false
    }
    });

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
