'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('sales', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
      id_user:{
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: 'users',
            key: 'id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
        },
      id_order:{
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
              model: 'orders',
              key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
          },
      produto_price_total: {
          type: Sequelize.FLOAT,
          allowNull: false
      },
    data:{
        type: Sequelize.DATEONLY,
        allowNull:false
    },
    created_at: {
        type: Sequelize.DATE,
        allowNull: false
    },
    updated_at: {
        type: Sequelize.DATE,
        allowNull: false
    }
});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
