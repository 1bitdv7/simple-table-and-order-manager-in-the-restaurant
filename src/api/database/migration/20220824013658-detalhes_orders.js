'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('detalhes_orders', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            id_order:{
                type: Sequelize.INTEGER,
                references: {
                    model: 'orders',
                    key: 'id'
                },
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
            },
            id_produto:{
              type: Sequelize.INTEGER,
              references: {
                  model: 'produtos',
                  key: 'id'
              },
              onUpdate: 'CASCADE',
              onDelete: 'CASCADE'
            },
            produto_qtd: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            total_order_price:{
              type: Sequelize.FLOAT,
                allowNull: false
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        });

    },

    async down(queryInterface, Sequelize) { /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    }
};
