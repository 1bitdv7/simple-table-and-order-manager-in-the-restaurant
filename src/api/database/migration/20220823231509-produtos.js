'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('produtos', {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false
            },
            id_categoria:{
              type: Sequelize.INTEGER,
              references: {
                  model: 'categorias',
                  key: 'id'
              },
              onUpdate: 'CASCADE',
              onDelete: 'CASCADE'
            },
            user_id:{
                type: Sequelize.INTEGER,
                references: {
                    model: 'users',
                    key: 'id'
                },
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE'
              },
            produto_name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            produto_price: {
                type: Sequelize.FLOAT,
                allowNull: false
            },
            produto_qtd: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            produto_descrisao:{
              type: Sequelize.STRING,
              allowNull: false
            },
            created_at: {
                type: Sequelize.DATE,
                allowNull: false
            },
            updated_at: {
                type: Sequelize.DATE,
                allowNull: false
            }
        });
    },

    async down(queryInterface, Sequelize) { /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    }
};
