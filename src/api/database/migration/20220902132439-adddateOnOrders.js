'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('orders', 'data',{
      type: Sequelize.DATEONLY,
      allowNull:false
  });
  },

  async down (queryInterface, Sequelize) {

  }
};
