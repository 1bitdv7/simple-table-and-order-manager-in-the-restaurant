const {sign, verify} = require("jsonwebtoken");
const Roles = require("../../model/Regras");


class AuthMiddleware {
    static async createTokenUser(data) {
        return await sign({}, `${process.env.SECRETE_KEY}`, {
            subject: `${
                data.userId
            }`,
            expiresIn: `${
                data.expire
            }`
        })
    }

    static Sesion(request,response,next){
        try {
            if( !request.session.user) return response.redirect('/logout')
            next()
        } catch (error) {
            response.status(401)
            throw error
        }
    }
    static async isAuth(request, response, next) {
        try {

            const tokenUserBearer = request.headers.authorization;

            if (! tokenUserBearer || ! tokenUserBearer.startsWith('Bearer')) {
                response.status(400)
                throw new Error(`error user not authenticated`)
            }

            const [, token] = tokenUserBearer.split(' ')

            if(!verify(token, `${process.env.SECRETE_KEY}`)){
                console.log(23234) 
            }

            const {sub} = await verify(token, `${process.env.SECRETE_KEY}`)

            request.userid = sub
            next()
        } catch (error) {
           if(error.message == 'jwt expired'){
               return response.status(400).json({message:'notAuthenticatedUser'})
           }
        }
    }


    static async Role(request, response, next) {
        try {

            const tokenUserBearer = request.headers.autorization;

            if (! tokenUserBearer || ! tokenUserBearer.startsWith('Bearer')) {
                response.status(401)
                throw new Error(`error user not authenticated`)
            }

            const [, token] = tokenUserBearer.split(' ')

            const { sub } = await verify(token, process.env.SECRETE_KEY)

            const levelSistemCheckRoles = await Roles.findOne({where:{userId: sub}})

            if(!levelSistemCheckRoles){
                response.status(401)
                throw new Error(`error user not authenticated`)
            }

            switch(levelSistemCheckRoles.level){
                case 2: 
                    next()
                break
                default:
                response.status(401)
                throw new Error(`error user access danied !`)
            }
            
        } catch (error) {
            throw error
        }
    }
}

module.exports = AuthMiddleware
