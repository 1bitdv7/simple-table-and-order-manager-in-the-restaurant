const multer = require('multer')

let storage = multer.diskStorage({
    destination: (req,file,cb)=>{
        cb(null,'./upload')
    },
    filename:(req,file,cb)=>{
        cb(null,Date.now()  +'--'+ file.originalname)
    }
})

let fileFilter = (req,file,cb)=>{
    if((file.mimetype).includes('jpeg') || (file.mimetype).includes('png') || (file.mimetype).includes('jpg') ){
        cb(null,true)
    }else{
        cb(null,false)
    }
}

let uploadFile= multer({
    storage:storage,
    fileFilter: fileFilter,
})


module.exports = uploadFile
