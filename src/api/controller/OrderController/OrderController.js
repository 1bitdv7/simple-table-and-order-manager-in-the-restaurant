const DetalhesOrder = require("../../model/DetalhesOrder")
const DetalhesOrderRepository = require("../../repositoriy/DetalhesOrderRepository/DetalhesOrderRepository")
const OrderRepository = require("../../repositoriy/OrderRepository/OrderRepository")
const ProdutosRepository = require("../../repositoriy/ProdutoRepository/ProdutoRepository")


class OrderController {

    static async DeleteOrder (request,response){
        try {
            if(request.params.id){
                const Order = new OrderRepository()
                await Order.DeleteOrder(request.params.id)
                return response.status(200).json({message: 'success'})
            }
        } catch (error) {
            response.status(400)
            throw error
        }
    }
    static async addOrderProdutos(request, response) {
        try {
            const {orderid, id} = request.params
            if (!orderid || !id) {
                return response.status(400).json({message: 'erro parametros invalidos '})
            }

            const produtoRepository = new ProdutosRepository()
            const findProdutos = await produtoRepository.findProdutosById(id)

            if (! findProdutos) {
                return response.status(400).json({message: ' Produto invalidos !'})
            }
            const OrderRepositorys = new OrderRepository()
            const Order = await OrderRepositorys.findAllOrderActiveById(orderid)

            if (! Order) {
                return response.status(400).json({message: ' Produto invalidos !'})
            }
            const OrderDetalhes = new DetalhesOrderRepository()
            const OrderDetalhesFind = await OrderDetalhes.findOrderdetalhesIdOrder(orderid, id)

            const today = new Date()

            if (! OrderDetalhesFind) {
                await OrderDetalhes.DetalhesOrderCreate({id_produto: id, id_order: orderid, produto_qtd: 1, total_order_price: findProdutos.produto_price,data:today.toLocaleDateString().replace(/[/]/g,'-').substring(0,10)})

                return response.status(200).json({message: 'success'})
            } else {
                await DetalhesOrder.update({
                    produto_qtd: OrderDetalhesFind.produto_qtd + 1,
                    total_order_price: OrderDetalhesFind.total_order_price + findProdutos.produto_price
                }, {
                    where: {
                        id_order: orderid,
                        id_produto: id
                    }
                })
                return response.status(200).json({message: 'success'})
            }


        } catch (error) {
            throw error
        }
    }

  
    static async order(request, response) {
        try {

            const {nome, email, nif} = request.body

            if (!nome || !email || !nif) {
                return response.status(400).json({message: 'parametros invalidos ou vazio'})
            }

            const orderRepository = new OrderRepository()

            const VerificarUsuarioComMesa = await orderRepository.findOrderAtive({nome, email})

            if (VerificarUsuarioComMesa) {
                return response.status(200).json({id: VerificarUsuarioComMesa.id})
            }
            const today = new Date()
            const newFormated = today.toLocaleDateString().replace(/[/]/g,'-').substring(0,10).split('-')
            const dateFormte  = `${newFormated[2]}-${newFormated[0]== 1 ? +'0'+newFormated[0] : newFormated[0]}-${newFormated[1]== 1 ?+'0'+newFormated[1] : newFormated[1] }`
            const ordersCreate = await orderRepository.CreateOrder({
                user_id: request.userid,
                nome: nome,
                email: email,
                status: 'ative',
                nif:nif,
                data: dateFormte
            })

            return response.status(200).json(ordersCreate)

        } catch (error) {
            response.status(400)
            throw error
        }
    }

    static async detalhesOrder(request, response) {
        try {
            const {id} = request.body
            const DetalhesOrder = new DetalhesOrderRepository()
            const detalhes = await DetalhesOrder.findOrderdetalhesId(id)
            return response.status(200).json(detalhes.rows)
        } catch (error) {
            throw error
        }
    }
    static async getAllOrders(request, response) {
        try {
            const orderRepository = new OrderRepository()
            const alllOrder = await orderRepository.findAllOrderActive()

            return response.status(200).json(alllOrder)
        } catch (error) {
            throw error
        }
    }
}


module.exports = OrderController
