const sequelize = require("sequelize")
const { Op } = require("sequelize")
const Configuracao = require("../../model/Configuracao")
const CategoriaRepositry = require("../../repositoriy/CategoriaRepository/CategoriaRepository")
const DetalhesOrderRepository = require("../../repositoriy/DetalhesOrderRepository/DetalhesOrderRepository")
const OrderRepository = require("../../repositoriy/OrderRepository/OrderRepository")
const ProdutosRepository = require("../../repositoriy/ProdutoRepository/ProdutoRepository")
const SalesRepository = require("../../repositoriy/SalesRepository/SalesRepository")
const UserRepository = require("../../repositoriy/UserRepository/UserRepository")
const { levelSystemModule } = require("../../util/util")


class RenderViewsController {
    static index(request, response) {
        response.status(200)
        response.render('user/login', {Title: "User Login | APP"})
    }

    static registerIndex(request, response) {
        response.status(200)
        response.render('user/register', {Title: "User Register | APP"})
    }

    static home(request, response) {
        response.status(200)
        const id = parseInt(request.session.user)
        response.render('home/index', {Title: 'Home | App',id,})
    }
     static logout (request,response){
        response.render('logout/logout', {Title: 'Saindo | App'})
     }

    static async addprodutosGet(request, response) {
        const categoriaRepository = new CategoriaRepositry()
        const fintAll = await categoriaRepository.findAll()
        response.status(200) 
        const id = parseInt(request.session.user)
        response.render('produtos/add', {
            Title: 'Adicionar Produtos | App',
            categorias: fintAll.rows,
            id: id
        })
    }

    static async produtosList(request, response) {
        const produtoRepository = new ProdutosRepository()
        const allProdutos = await produtoRepository.findAllProdutos()
        response.status(200)
        const id = parseInt(request.session.user)
        response.render('produtos/produtos', {
            Title: 'Produtos | App',
            data: allProdutos.rows,
            id:id
        })
    }
    static async indexAdd(request, response) {
        try {
            const produtoRepository = new ProdutosRepository()
            const allProdutos = await produtoRepository.findAllProdutos()
            response.status(200)
            const id = parseInt(request.session.user)
            response.render('order/order', {
                    Title: `Order n#${
                    request.params.id
                } | App`,
                data: allProdutos.rows,
                id: id
            })
        } catch (error) {
            throw error
        }
    }

    static async preview(request, response) {
        const OrderDetalhe = new DetalhesOrderRepository()
        const Order = new OrderRepository()

        const OrderFind = await Order.findAllOrderActiveById(request.params.id)

        const findOrder = await OrderDetalhe.findOrderdetalhesId(request.params.id)
        const Obj = [] || ''
        let total = 0
        let orderid = 0

        
        for (const key in findOrder.rows) {
            orderid = orderid + parseInt(findOrder.rows[key].id)
            Obj.push({
                produto_name: findOrder.rows[key]['produtos'].produto_name,
                produto_qtd: findOrder.rows[key].produto_qtd,
                total_order_price: findOrder.rows[key].total_order_price
            })
            total = total + parseFloat(findOrder.rows[key].total_order_price)
        }
        const data = new Date(OrderFind.created_at).toLocaleDateString()
        
        response.status(200)
        response.render('order/preview', {
            data: Obj,
            detlahes:{
                nif: OrderFind.nif,
                data:data,
                nome: OrderFind.email,
                atendente: OrderFind['users'].nome,
            },
            Title: `Client Order n#${
                request.params.id
            }| App`,
            total: total,
            fatura: orderid
        })
    }
    static async OrderFine(request, response) {
        const OrderDetalhe = new DetalhesOrderRepository()
        const Order = new OrderRepository()

        const OrderFind = await Order.findAllOrderActiveById(request.params.id)

        const findOrder = await OrderDetalhe.findOrderdetalhesId(request.params.id)
        const Obj = [] || ''
        let total = 0
        let orderid = 0

        if(OrderFind.status == 'desativado'){
            return response.status(404).render('order/status',{
                Title: 'Operacao Invalida',
                message: 'Operacao Invalida esta fatura ja foi processada tenta Reactivar'
            })
        }


        for (const key in findOrder.rows) {
            orderid = orderid + parseInt(findOrder.rows[key].id)
            Obj.push({produto_name: findOrder.rows[key]['produtos'].produto_name,
                produto_qtd: findOrder.rows[key].produto_qtd,
                total_order_price: findOrder.rows[key].total_order_price
            })
            total = total + parseFloat(findOrder.rows[key].total_order_price)
        }

        const today = new Date()
        const newFormated = today.toLocaleDateString().replace(/[/]/g,'-').substring(0,10).split('-')
        const dateFormte  = `${newFormated[2]}-${newFormated[0]== 1 ? +'0'+newFormated[0] : newFormated[0]}-${newFormated[1]== 1 ?+'0'+newFormated[1] : newFormated[1] }`

        const Salesrepository = new SalesRepository()

        await Salesrepository.createSales({
            data: dateFormte,
            id_order: request.params.id,
            id_user: request.session.user,
            produto_price_total: total
        })



        await Order.UpdateOrder({
            status: 'desativado'
        }, {
            where: {
                id: request.params.id
            }
        })
        const data = new Date(OrderFind.created_at).toLocaleDateString()
        
        response.status(200)
        response.render('order/fine', {
            data: Obj,
            detlahes:{
                nif: OrderFind.nif,
                data:data,
                nome: OrderFind.email,
                atendente: OrderFind['users'].nome,
                troco: request.query.tl ? request.query.tl : "0.00",
            },
            Title: `Client Order n#${
                request.params.id
            }| App`,
            total: total,
            fatura: orderid
        })
    }

    static async usuarios (request,response){
        try {
            const userRepository = new UserRepository()
            const userFindAll = await userRepository.findUserAll()
            const data = [] || ''
            const id = parseInt(request.session.user)
            for (const key in userFindAll.rows) {
               data.push({
                id: userFindAll.rows[key].id,
                foto: userFindAll.rows[key]['fotos'].foto,
                level: levelSystemModule(userFindAll.rows[key]['regras'].levels),
                nome: userFindAll.rows[key].nome,
                email: userFindAll.rows[key].email
               })
            }

            return response.status(200).render('usuarios/usuarios',{Users:data,id:id,Title: 'Lista de Usuarios | APP'})
        } catch (error) {
            throw error.message
        }
    }


    static async configuracao(request, response) {
        try {
            const configuracao = await Configuracao.findAll()
            const id = parseInt(request.session.user)
            return response.status(200).render('configuracao/config', {id:id,Title: 'Configuracao da Empresa',config:configuracao})
        } catch (error) {
            throw error
        }
    }
    static async EditarProdutoView(request, response) {
        try {
            const id = request.params.id

            if (! id) {
                response.status(404)
                return response.render('404/404', {
                    Title: 'Page not Found',
                    status: 404
                })
            }
            const produtosRepository = new ProdutosRepository()
            const findProduto = await produtosRepository.findProdutosById(id)

            if (! findProduto) {
                response.status(404)
                return response.render('404/404', {
                    Title: 'Produtos nao Encontrado',
                    status: 404
                })
            }
            let ids,categoria = ''
            if(findProduto['categorias']== null){
                const repositoriyCategoria = new CategoriaRepositry()
                categoria = await repositoriyCategoria.findAll()
            }else{
                categoria = findProduto['categorias'].nome
                ids = findProduto['categorias'].id
            }

            response.status(200)
            const sid = parseInt(request.session.user)
            response.render('produtos/editar', {
                    Title: `Editar Produto #${
                    findProduto.id
                }`,
                produto: findProduto,
                categoria:categoria,
                idp:ids,
                id:sid
            })

           

        } catch (error) {
            throw error
        }
    }

    static async relatorio(request,response){

        if(request.session.user == undefined){
            //response.redirect('/')
          //  return
        }

        const id = parseInt(request.session.user)
        return response.render('ralatorio/relatorio', {
            id:id,
            Title: 'Relatorio | App'
            
        })
    }
    static async fechodiario(request,response){
        const id = request.params.id
        const today = new Date()
        const repositoriy = new OrderRepository()
        console.log(today.toISOString())
        const Orders = await repositoriy.findOrderToday(today.toISOString(),id)

        const Obj = [] || ''
        let valor = 0
        return response.render('ralatorio/relatorios', {
            Title: 'Relatorio do Dia  | App',
            data: Orders.rows,
            valor:valor
        })
    }

    static async usuarioEditar(request, response) {
        const {id} = request.params

        if(!id){}

        const repositoriy = new UserRepository()
        const findUser = await repositoriy.findUserById(id)

        const ids = parseInt(request.session.user)
        return response.render('usuarios/add',{
            Title: 'Editar Usuario | App',
            editar: findUser,
            id:ids
        })
    }
    static async usuarioAdicionar(request, response) {
        const id = parseInt(request.session.user)
        response.render('usuarios/add', {
            Title: 'Adicionar Usuario | App',
            editar: [],
            id: id
        })
    }
    static async ordersAtive(request, response) {
        const OrderRepositorys = new OrderRepository()
        const FindAllOrder = await OrderRepositorys.findAllOrderActive()
        const id = parseInt(request.session.user)
        response.status(200)
        response.render('order/orders', {
            Title: 'Lista de mesas Ativa | App',
            Order: FindAllOrder.rows,
            id:id
        })
    }
    static async orderlist(request, response) {
        const OrderRepositorys = new OrderRepository()
        const FindAllOrder = await OrderRepositorys.findAllOrderDesative()
         const id = parseInt(request.session.user)
        response.status(200)
        response.render('order/orderlist', {id,Title: 'Historico de Mesas | App',Order: FindAllOrder.rows,id: request.session.user})
    }
    static ordersById(request, response) {
        const id = parseInt(request.session.user)
        response.status(200)
        response.render('order/order', {
                Title: `Order n#${
                request.params.id
            } | App`,
            id:id
        })
    }


    static async vendas(request, response) {
        try {
            const id = parseInt(request.session.user)
            return response.status(200).render('ralatorio/vendas/index.ejs',{
                Title: 'Relatorio de Vendas',
                id: id
            })
        } catch (error) {
            throw error
        }
    }
    
    static async IndexVendas(request,response){
        try {

            const params = request.query.data
            let query = ''
            const Salesrepository = new SalesRepository()

            const today = new Date()
            
            if( !params){

                return response.renderer('order/status',{
                    Title: 'Operacao Invalida',
                    message: 'Operacao Invalida !'

                })
            }

            if( params == 'last'){
                let Last = `${today.getFullYear()}-${today.getMonth()}-${(parseInt(today.getDate()) - 7)}`
                query = await Salesrepository.findAllCond({
                    start: Last,
                    end: Last
                })

            }


            switch(parseInt(params)){
                case 1:
                    const datenow = new Date().setHours(0,0,0,0)
                    query = await Salesrepository.findAllCond({
                        start: datenow,
                        end: today.toISOString(),
                    })

                break

                case 2:
                const date = new Date().setHours(0,0,0,0).toFixed()
                let endDate = new Date().setDate()
                query = await Salesrepository.findAllCond({
                    start: date,
                    end: endDate
                })
                
                break

                case 7:
                    let LastStart = `${today.getFullYear()}-${today.getMonth()}-${(parseInt(today.getDate()) - 7)}`
                    let LastEnd = `${today.getFullYear()}-${today.getMonth()}-${today.getDate()}`
                    query = await Salesrepository.findAllCond({
                        start: LastStart,
                        end: LastEnd
                    })

                break
                case 30:
                    let thisStart = `${today.getFullYear()}-${today.getMonth()}-01`
                    let thisEnd = `${today.getFullYear()}-${today.getMonth()}-${today.getDate()}`
                    query = await Salesrepository.findAllCond({
                        start: thisStart,
                        end: thisEnd
                    })

                break

            }

            return response.status(200).render('ralatorio/vendas/vendas.ejs',{
                Title: 'Relatorio de Vendas',
                data: query.rows,
                error: [],
                periudo: params
            })
            
        } catch (error) {
            throw error
        }
    }

    static async FunctionarioRelatorio (request,response){
        try {
            
            const { data,funcionario } = request.query
            let query = ''
            const Salesrepository = new SalesRepository()

            const today = new Date()
            
            if( !data){

                return response.renderer('order/status',{
                    Title: 'Operacao Invalida',
                    message: 'Operacao Invalida !'

                })
            }

            if( data == 'last'){
                let Last = `${today.getFullYear()}-${today.getMonth()}-${(parseInt(today.getDate()) - 7)}`
                query = await Salesrepository.findAllCondByUser({
                    start: Last,
                    end: Last,
                    id: funcionario
                })

            }


            switch(parseInt(data)){
                case 1:
                    let x1 = `${today.getFullYear()}-${today.getMonth()}-${today.getDate()}`
                    query = await Salesrepository.findAllCondByUser({
                        start: x1,
                        end: x1,
                        id: funcionario
                    })

                break

                case 2:
                let endDate = `${today.getFullYear()}-${today.getMonth()}-${(parseInt(today.getDate()) - 1)}`
                query = await Salesrepository.findAllCondByUser({
                    start: endDate,
                    end: endDate,
                    id: funcionario
                })
                
                break

                case 7:
                    let LastStart = `${today.getFullYear()}-${today.getMonth()}-${(parseInt(today.getDate()) - 7)}`
                    let LastEnd = `${today.getFullYear()}-${today.getMonth()}-${today.getDate()}`
                    query = await Salesrepository.findAllCondByUser({
                        start: LastStart,
                        end: LastEnd,
                        id: funcionario
                    })

                break
                case 30:
                    let thisStart = `${today.getFullYear()}-${today.getMonth()}-01`
                    let thisEnd = `${today.getFullYear()}-${today.getMonth()}-${today.getDate()}`
                    query = await Salesrepository.findAllCondByUser({
                        start: thisStart,
                        end: thisEnd,
                        id: funcionario
                    })

                break

            }
            const id = parseInt(request.session.user)
            return response.status(200).render('ralatorio/funcionarios/funcionarios.ejs',{
                Title: 'Relatorio de Vendas por Funcionario',
                data:query.rows,
                error: [],
                id: id,
                periudo: data
            })
        } catch (error) {
            throw error
        }
    }

    static async FunctionarioIndex (request,response){
        try {
            const id = parseInt(request.session.user)
            const users = new UserRepository()
            const getUsers = await users.findUserAll()
            
            return response.status(200).render('ralatorio/funcionarios/index.ejs',{
                Title: 'Relatorio de Vendas por Funcionario',
                data: getUsers.rows,
                error: [],
                id: id
            })
        } catch (error) {
            throw error
        }
    }







}

module.exports = RenderViewsController
