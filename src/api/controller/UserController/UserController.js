const AuthMiddleware = require("../../middleware/auth/auth")
const UserRepository = require("../../repositoriy/UserRepository/UserRepository")
const bcrypt = require('bcryptjs')
const Fotos = require("../../model/Fotos")
const Regras = require("../../model/Regras")
const {levelSystemModule} = require("../../util/util")
const Configuracao = require("../../model/Configuracao")
const Users = require("../../model/Users")

class UserController {

    static async configuracao(request, response) {
        try {
            const {
                name,
                endereco,
                numero,
                email,
                nif,
                mensagem,
                desconto
            } = request.body

          const  configuracao = await Configuracao.create({
                name,
                endereco,
                numero,
                email,
                nif,
                file:request.file.filename,
                mensagem,
                desconto
            })
            return response.status(200).redirect('/configuracao')
        } catch (error) {
            throw error.message
        }
    }

    static async login(request, response) {
        try {
            const {email, password} = request.body

            if (!email || !password) {

                return response.status(400).json({message: "email or password invalid !s"})
            }
            const userrepository = new UserRepository()
            const User = await userrepository.findUserByEmail(email)

            if (User) {

                const comparepassword = await bcrypt.compare(password, User.password)
                if (! comparepassword) {

                    return response.status(400).json({message: "email or password invalid !s"})
                }

                const data = {
                    userId: User.id,
                    expire: '1d'
                }
                const token = await AuthMiddleware.createTokenUser(data)

                const data_atual = new Date()

                response.cookie('ps',{ps:User.nome},{
                    maxAge:5000,
                    expires: new Date(data_atual.getFullYear(),
                    data_atual.getMonth(),
                    data_atual.getDate() + parseInt(7)).toISOString(),
                    secure: true,
                    httpOnly: true,
                    sameSite:'lax'
                })

                request.session.user = User.id;

                return response.status(200).json({
                    token: token,
                    email: User.email,
                    name: User.nome,
                    foto: User['fotos'].foto,
                    regra: levelSystemModule(User['regras'].levels)
                })

                
            } else {
                return response.status(400).json({message: 'email or password invalid !'})
            }
        } catch (error) {
            response.status(400)
            throw error
        }
    }


    static async UpdateUser(request, response){
        try {
           
             const password = request.body.password 
             const email = request.body.email
             const nome = request.body.nome
             const id = request.body.id
            
            if(!nome || !email || !password){
                return response.status(400).json({
                    message:"Erro algo deu errado !"
                })
            }
            const passwordhash = await bcrypt.hash(password, 8)

            const repositoriy = new UserRepository()

            await Users.update({
                nome:nome,
                email: email,
                password: passwordhash
            },{where: {id: id}})
          
            return response.status(200).json({message: 'success'})
        } catch (error) {
            response.status(400)
            throw error
        }  
    }
    static async createUser(request, response) {
        try {

            const {nome, email, password, level} = request.body

            if (!nome || !email || !password) {

                return response.status(400).json({message: "parametros invalidos ou vazios !"})

            }
            const userrepository = new UserRepository()
            const userMailverified = await userrepository.findUserByEmail(email)

            if (userMailverified) {
                return response.status(400).json({message: "email existente , tenta outro!"})
            }

            const passwordhash = await bcrypt.hash(password, 8)

            const fotoUser = await Fotos.create({foto: request.file.filename})

            const rulles = await Regras.create({levels: level})

            const user = await userrepository.createUser({
                nome,
                email,
                password: passwordhash,
                rules_id: rulles.id,
                foto_id: fotoUser.id
            })


            return response.status(200).redirect('/usuarios')

        } catch (error) {
            throw error
        }
    }
}

module.exports = UserController
