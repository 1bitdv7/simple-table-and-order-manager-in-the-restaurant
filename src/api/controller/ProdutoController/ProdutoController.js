const DetalhesOrder = require("../../model/DetalhesOrder")
const Produtos = require("../../model/Produtos")
const DetalhesOrderRepository = require("../../repositoriy/DetalhesOrderRepository/DetalhesOrderRepository")
const ProdutosRepository = require("../../repositoriy/ProdutoRepository/ProdutoRepository")


class ProdutoController {

    static async deleteProduto(request, response) {
        try {
            if (!request.params.id) {
                return response.status(400).json({message: "Error id Invalid "})
            }
            const produtoRepository = new ProdutosRepository()
            await produtoRepository.DeleteProdutos(request.params.id)
            return response.status(200).json({message: "Error id Invalid "})
        } catch (error) {
            response.status(`400`)
            throw error.message
        }
    }

    static async searchProdutos(request, response) {
        try {
            const search = request.body.query
            if (! search) {
                return response.status(404).json({message: ""})
            }
            const produtoRepository = new ProdutosRepository()
            const searchs = await produtoRepository.search(search)
            return response.status(200).json({json: searchs.rows})
        } catch (error) {
            response.status(400)
            throw error.message
        }
    }

    static async addprodutos(request, response) {
        try {
            const {
                produto_name,
                produto_price,
                produto_qtd,
                produto_descrisao,
                id_categoria
            } = request.body
            if (!produto_qtd || !produto_name || !produto_price || !produto_descrisao) {
                return response.status(400).json({message: "existe campos vazios"})
            }
            const produtoRepository = new ProdutosRepository()
            const fintprodutoNome = await produtoRepository.findProdutosByname(produto_name)

            if (fintprodutoNome) {
                return response.status(400).json({message: "esse produto ja existe !"})
            }

            await produtoRepository.CreateProdutos({
                produto_name,
                user_id: request.userid,
                produto_price,
                id_categoria,
                produto_qtd,
                produto_descrisao
            })

            return response.status(200).json({message: "success"})

        } catch (error) {
            throw error
        }
    }

    static async Updateprodutos(request, response) {
        try {
            const {
                produto_name,
                produto_price,
                produto_qtd,
                produto_descrisao,
                id_categoria,
                id
            } = request.body
            if (!produto_qtd || !produto_name || !produto_price || !produto_descrisao) {
                return response.status(400).json({message: "existe campos vazios"})
            }

            const produtoRepository = new ProdutosRepository()
            const fintprodutoNome = await produtoRepository.findProdutosById(id)

            if (! fintprodutoNome) {
                return response.status(400).json({message: "produto invalido!"})
            }
            await Produtos.update({
                produto_name,
                produto_price,
                id_categoria,
                produto_qtd,
                produto_descrisao
            }, {
                where: {
                    id: id
                }
            })

            return response.status(200).json({message: 'success'})

        } catch (error) {
            response.status(400)
            throw error.message
        }
    }
    static async quantityRemoveOrproduto(request, response) {
        try {
            const {id, orderid} = request.body
            const detalhesOrderRepository = new DetalhesOrderRepository()
            const produto = new ProdutosRepository()
            const fintProdutox = await produto.findProdutosById(id)
            const findProduto = await detalhesOrderRepository.findOrderdetalhesIdOrder(orderid, id)
            if (findProduto.produto_qtd > 1) {
                let produto_qtd = findProduto.produto_qtd - 1
                let total_order_price = findProduto.total_order_price - fintProdutox.produto_price
                await DetalhesOrder.update({
                    produto_qtd: produto_qtd,
                    total_order_price: total_order_price
                }, {
                    where: {
                        id_order: orderid,
                        id_produto: id
                    }
                })
                return response.status(200).json({message: 'success'})
            } else {
                await DetalhesOrder.destroy({
                    where: {
                        id_order: orderid,
                        id_produto: id
                    }
                })

                return response.status(200).json({message: 'success'})
            }
        } catch (error) {
            response.status(400)
            throw error.message
        }
    }

}

module.exports = ProdutoController
