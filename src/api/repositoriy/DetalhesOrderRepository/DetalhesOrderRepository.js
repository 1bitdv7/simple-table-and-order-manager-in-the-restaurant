const DetalhesOrder = require("../../model/DetalhesOrder")


class DetalhesOrderRepository {
    async DetalhesOrderCreate(data) {
        return await DetalhesOrder.create(data)
    }


    async findOrderdetalhesId(id) {
        return await DetalhesOrder.findAndCountAll({
            where: {
                id_order: id
            },
            include: [
                {
                    association: 'produtos'
                }
            ]
        })
    }

    async DetalhesOrderFind(order_id) {

        return await DetalhesOrder.findAndCountAll({
            where: {
                id_order: order_id
            },
            order: [
                ['id', 'DESC']
            ],
            include: [
                {
                    association: 'produtos'
                }, {
                    association: 'orders'
                }
            ]
        })
    }

    async findOrderdetalhesIdOrder(id_order, id) {
        return await DetalhesOrder.findOne({
            where: {
                id_order: id_order,
                id_produto: id
            }
        })
    }
    async DetalhesPagination(data) {
        return await DetalhesOrder.findAndCountAll({
            limit: data.totalItens,
            offset: data.page,
            order: [
                ['id', 'DESC']
            ],
            include: [
                {
                    association: 'users'
                }
            ]
        })
    }
    async UpdateOrder(data) {
        return await DetalhesOrder.update(data)
    }
    async DeleteOrder(id) {
        return await DetalhesOrder.destroy({
            where: {
                id: id
            }
        })
    }

}

module.exports = DetalhesOrderRepository
