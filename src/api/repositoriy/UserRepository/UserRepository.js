const Users = require("../../model/Users")


class UserRepository {
    async createUser(data) {
        return await Users.create(data)
    }
    async findUserById(id) {
        return await Users.findByPk(id,{include:[{association: 'regras'},{association:'fotos'}]})
    }

    async findUserByEmail(email){
        return await Users.findOne({where:{email:email},
            include:[{association: 'regras'},{association:'fotos'}]})
    }
    async getUsersPagination(data) {
        return await Users.findAndCountAll({
            limit: data.totalItens,
            offset: data.page,
            order: [
                ['id', 'DESC']
            ]
        })
    }
    async findUserAll() {
        return await Users.findAndCountAll({
            limit: 10,
            offset: 0,
            order: [
                ['id', 'DESC']
            ],
            include:[{association: 'regras'},{association:'fotos'}]
        })
    }

    async updateUser({date},{where}) {
        return await Users.update(date,where)
    }
    async deleteUser(id) {
        return await Users.destroy({
            where: {
                id: id
            }
        })
    }
}

module.exports = UserRepository
