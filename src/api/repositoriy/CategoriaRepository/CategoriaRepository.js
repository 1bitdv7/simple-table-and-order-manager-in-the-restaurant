
const Categorias = require("../../model/Categoria")
const Users = require("../../model/Users")


class CategoriaRepositry {
    async createUser(data) {
        return await Users.create(data)
    }
    async findAll() {
        return await Categorias.findAndCountAll()
    }

    async findUserByEmail(email){
        return await Users.findOne({where:{email:email}})
    }
    async getUsersPagination(data) {
        return await Users.findAndCountAll({
            limit: data.totalItens,
            offset: data.page,
            order: [
                ['id', 'DESC']
            ]
        })
    }
    async findUserAll() {
        return await Users.findAll({
            limit: 10,
            offset: 0,
            order: [
                ['id', 'DESC']
            ]
        })
    }

    async updateUser({date}) {
        return await Users.update(date)
    }
    async deleteUser(id) {
        return await Users.destroy({
            where: {
                id: id
            }
        })
    }
}

module.exports = CategoriaRepositry
