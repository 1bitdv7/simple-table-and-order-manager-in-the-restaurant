const { Op } = require("sequelize")
const Sales = require("../../model/Sales")


class SalesRepository {
    async createSales(data) {
        return await Sales.create(data)
    }

    async findAllSalesByDate(dataStart, dataEnd) {
        return await Sales.findAndCountAll({
            where: {},
            order: [
                ['id', 'DESC']
            ]
        })
    }


    async findAllSales() {
        return await Sales.findAll({
            order: [
                ['id', 'DESC']
            ]
        })
    }

    async findAllCond(data) {
        const { start,end} = data
        return await Sales.findAndCountAll({
                where:{
                    created_at: {
                        [Op.between]: [start,end]
                    }
                },
                order:[
                    ['id', 'DESC']
                ],
                include: [
                    {
                        association: "users"
                    }, {
                        association: 'orders'
                    }
                ]
            }
    )
}

async findAllCondByUser(data) {
    const { start,end,id} = data
    return await Sales.findAndCountAll({
            where:{
                created_at: {
                    [Op.between]: [start,end]
                },
                id_user:id
            },
            order:[
                ['id', 'DESC']
            ],
            include: [
                {
                    association: "users"
                }, {
                    association: 'orders'
                }
            ]
        }
)
}

async findSalesByUser(id, data) {
    return await Sales.findAndCountAll({
        where: {
            data: data,
            id_user: id
        },
        order: [
            ['id', 'DESC']
        ],
        include: [
            {
                association: "users"
            }, {
                association: 'produtos'
            }
        ]
    })
}}module.exports = SalesRepository
