const { Op, Sequelize } = require("sequelize")
const Order = require("../../model/Orders");


class OrderRepository {
    async CreateOrder(date) {
        return await Order.create(date)
    }
    async UpdateOrder(data = {}, where = {}) {
        return await Order.update(data, where)
    }
    async findAllOrderActiveById(id) {
        return await Order.findByPk(id, {
            include: [
                {
                    association: 'users'
                }
            ]
        })
    }
    async findOrderAtive(data = {}) {
        return await Order.findOne({
            where: {
                email: data.email,
                nome: data.nome,
                status: 'ative'
            }
        })
    }
    async findAllOrderActive() {
        return await Order.findAndCountAll({
            where: {
                status: 'ative'
            },
            limit: 20,
            offset: 0,
            order: [
                ['id', 'DESC']
            ]
        })
      
        
    }
    async findOrderToday(data,id) {
        const today = new Date().setHours(0,0,0,0)
        return await Order.findAndCountAll({
            where: {
                created_at: {
                        [Op.between]: [today,data]
                },
                'user_id':id
            },
            include: [
                {
                    association: 'users'
                }
            ]
        })
    }
    async findAllOrderDesative() {
        return await Order.findAndCountAll({
            where: {
                status: 'desativado'
            },
            limit: 20,
            offset: 0,
            order: [
                ['id', 'DESC']
            ]
        })
    }
    async findAllOrderPagination(data) {
        return await Order.findAndCountAll({
            limit: data.totalItens,
            offset: data.age,
            order: [
                ['id', 'DESC']
            ],
            include: [
                {
                    association: "users"
                }
            ]
        })
    }
    async findAllOrder() {
        return await Order.findAll({
            limit: 10,
            offset: 0,
            order: [
                ['id', 'DESC']
            ],
            include: [
                {
                    association: "users"
                }
            ]
        })
    }
    async DeleteOrder(Id) {
        return await Order.destroy({
            where: {
                id: Id
            }
        })
    }
}


module.exports = OrderRepository
