const {Op} = require("sequelize")
const Produtos = require("../../model/Produtos")


class ProdutosRepository {
    async CreateProdutos(date) {
        return await Produtos.create(date)
    }
    async UpdateProdutos({date}) {
        return await Produtos.update(date)
    }
    async findProdutosByname(name) {
        return await Produtos.findOne({
            where: {
                produto_name: name
            }
        })
    }
    async findProdutosById(id) {
        return await Produtos.findByPk(id, {
            include: [
                {
                    association: 'categorias'
                }
            ]
        })
    }
    async search(campo) {
        return await Produtos.findAndCountAll({
            where:{
                produto_name: {
                    [Op.like]: '%' + campo + '%'
                }
            }
        })
    }
    async findAllProdutosPagination(data) {
        return await Produtos.findAndCountAll({
            limit: data.totalItens,
            offset: data.age,
            order: [
                ['id', 'DESC']
            ],
            include: [
                {
                    association: "users"
                }
            ]
        })
    }
    async findAllProdutos() {
        return await Produtos.findAndCountAll({
            order: [
                ['id', 'DESC']
            ],
            include: [
                {
                    association: "users"
                }
            ]
        })
    }
    async DeleteProdutos(Id) {
        return await Produtos.destroy({
            where: {
                id: Id
            }
        })
    }
}

module.exports = ProdutosRepository
