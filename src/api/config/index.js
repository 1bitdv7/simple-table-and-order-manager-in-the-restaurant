module.exports = {
    dialect: 'mysql',
    host: 'localhost',
    username: 'root',
    password: '',
    database: 'restourante_system',
    define:{
        timestamp: true,
        underscored: true
    }
}