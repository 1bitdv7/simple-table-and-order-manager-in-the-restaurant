require('dotenv').config
const express = require('express')
const path = require('path')
const Router = require('./api/routes/Routes.js')
const {handlerError} = require('./api/util/util.js')
require('./api/database/database')
require('express-async-handler')

const app = express()

app.use(express.json())

app.set('view engine','ejs')
app.set('views',path.join(__dirname+'/frontend/view'))
app.use(express.static('public'))


const port = process.env.PORT || 9000

Router(app)
app.use(handlerError)

app.listen(port,()=> console.log(`Server listen in ${port}`))