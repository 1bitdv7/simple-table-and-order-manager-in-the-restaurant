'use strict'
//const fetch = require("./fetch");



async function fetchall(url, data = {}, method, authorization) {
    const response = await fetch(url, {
        method: method, 
        cache: 'no-cache', 
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${authorization}`
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
        body: JSON.stringify(data) 
    });

    return response.json()
}

async function deleteProduto(a){
    const dataUser = JSON.parse(window.localStorage.getItem('User'))
    const produtoId = a.getAttribute('name')
    $(`#produtos${a.getAttribute('id')}`).fadeOut('fast')
    const fetch = await fetchall(`${url}/produto/${produtoId}`,{},'DELETE',dataUser.token)
    if(fetch.message == 'success')
        $(a).fadeOut('fast')
}