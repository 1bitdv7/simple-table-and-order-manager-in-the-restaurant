
const url = "http://localhost:9000";
const dataUser = JSON.parse(window.localStorage.getItem('User'))
 async function fetchall(url, data = {}, method, authorization) {
    const response = await fetch(url, {
        method: method, // *GET, POST, PUT, DELETE, etc.
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${authorization}`
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });

    return response.json()
}
 const detalhes  = document.querySelectorAll('ul.ul-detalhes') 
 const totalValorRelatorio = document.getElementById('totalValorRelatorio')
 let total_amount = 0 
 detalhes.forEach(element=>{
     const detalhesSection = document.getElementById(element.getAttribute('id'))
     const totalList = element.querySelector(`h6`)
     fetchall(`${url}/detalhes/`,{
        id:detalhesSection.getAttribute('name') 
       },'POST',dataUser.token).then(data=>{
        if (data.message == 'notAuthenticatedUser') {
            window.localStorage.clear()
            window.location.assign(url)
            return
        }
        const date = JSON.stringify(data)
        const Objects = [] || ''
        let ValoreDue = 0
        if(date.length > 0){
        for (const key in data) {
            const div = document.createElement('li')
            div.style.padding = '5px'
            div.className = `${data[key]['id_produto']}-li`
            div.setAttribute('id',`${key}-li`)
            let Total = data[key]['produtos']['produto_price'] * data[key]['produto_qtd']
            div.textContent = `${data[key]['produtos']['produto_name']} - Quantidade ${data[key]['produto_qtd']} - Valor Total ${Total} akz` 
            ValoreDue = ValoreDue + Total
                    Objects.push(div)
                 }
            }
           
            for (const key in Objects) {
                detalhesSection.append(Objects[key])
            }
            total_amount = total_amount + ValoreDue
            totalList.textContent = `Total Gasto: ${Number(ValoreDue).toFixed(2)} akz`
            totalValorRelatorio.textContent = `${Number(total_amount).toFixed(2)} Akz`
       })
       
 })

 