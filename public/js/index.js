if(window.localStorage.getItem('User')){
  const host = 'http://' + window.location.host + '/home'
  window.location.assign(host)
}


async function postData(url = '', data = {}) {
    const response = await fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    });

    console.log(response)
    return response.json(); // parses JSON response into native JavaScript objects
  }

  const form = document.getElementById('form')
  const url = 'http://localhost:9000/login';

  form.addEventListener('submit',async (e)=>{
      const btn = document.getElementById('btnsubmit')
      btn.setAttribute('disabled','true')
      btn.textContent = 'Entrando..'
      e.preventDefault()
      let email = document.getElementById('typeEmailX').value
      let password = document.getElementById('typePasswordX').value
      postData(url, {email,password})
            .then((data) => {
                if(data.message){
                    let status  = document.getElementById('status')
                    status.textContent = ''
                    status.textContent = data.message
                    btn.removeAttribute('disabled')
                    btn.textContent = 'Entrar'
                    return
                }
                window.localStorage.setItem('User',JSON.stringify(data))
                const host = 'http://'+ window.location.host + '/orders'   
                window.location.assign(host)

            });
  })
  

  module.exports = postData