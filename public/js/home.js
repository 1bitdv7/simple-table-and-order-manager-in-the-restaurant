
const url = "http://localhost:9000";

document.addEventListener("DOMContentLoaded", function(event) {
   
    const showNavbar = (toggleId, navId, bodyId, headerId) =>{
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId),
    bodypd = document.getElementById(bodyId),
    headerpd = document.getElementById(headerId)
    
    // Validate that all variables exist
    if(toggle && nav && bodypd && headerpd){
    toggle.addEventListener('click', ()=>{
    // show navbar
    nav.classList.toggle('show')
    // change icon
    toggle.classList.toggle('bx-x')
    // add padding to body
    bodypd.classList.toggle('body-pd')
    // add padding to header
    headerpd.classList.toggle('body-pd')
    })
    }
    }
    
    showNavbar('header-toggle','nav-bar','body-pd','header')
    
    /*===== LINK ACTIVE =====*/
    const linkColor = document.querySelectorAll('.nav_link')
    
    function colorLink(){
    if(linkColor){
    linkColor.forEach(l=> l.classList.remove('active'))
    this.classList.add('active')
    }
    }
    linkColor.forEach(l=> l.addEventListener('click', colorLink))
    
     // Your code to run since DOM is loaded and ready
    });
if (window.localStorage.getItem('User')) {
    const username = document.getElementById('Username')
    const dataUser = JSON.parse(window.localStorage.getItem('User'))
    const imageProfile = document.getElementById('imageprofile')
    imageProfile.setAttribute('src',`/upload/${dataUser.foto}`) ;
    

    if(dataUser.regra == 'Garsonet'){
        const allMenu = document.querySelectorAll('a.types')
        allMenu.forEach((a)=>{
            const element = document.getElementById(a.getAttribute('id'))
            element.style.display = 'none'
        })
        document.getElementById('finalizar').style.display = 'none'
    }else if(dataUser.regra === 'Atendente'){
        const allMenu = document.querySelectorAll('a.at')
        allMenu.forEach((a)=>{
            const element = document.getElementById(a.getAttribute('id'))
            element.style.display = 'none'
        })
       
    }

    document.getElementById('ClickLogout').addEventListener('click',()=>{
        window.localStorage.clear()
        const host = 'http://'+ window.location.host + '/orders'   
        window.location.assign(host)
    })

   

    async function fetchall(url, data = {}, method, authorization) {
        const response = await fetch(url, {
            method: method, // *GET, POST, PUT, DELETE, etc.
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${authorization}`
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });

        return response.json()
    }

    if(document.getElementById('formOrderCreateUsuarios')){
        const FormCreateUser = document.querySelector('form[id="formOrderCreateUsuarios"]')
        FormCreateUser.addEventListener('submit', async(a)=>{
            const buttonSubmit = document.getElementById('buttonCriarUsuarios')
            buttonSubmit.setAttribute('disabled', 'true')
            buttonSubmit.textContent = 'Criando usuario..'
            a.preventDefault()
            fetchall(`${url}/register`,{
                name: document.getElementById('name').value,
                email: document.getElementById('email').value,
                password: document.getElementById('password').value,
                level: document.getElementById('level').value,
                file: document.getElementById('file').value
            },'POST').then(data=>{
                alert(data)
            }).catch(error=>{

            })
            //alert(0)
        })
    }
   
    if( document.getElementById('formOrderCreateUsuariosdfUpdate')){
        document.getElementById('formOrderCreateUsuariosdfUpdate').addEventListener('submit',(form)=>{
            form.preventDefault()
            const [,,, orderId,] = (window.location.pathname).split('/')
            const nome = document.getElementById('name')
            const email = document.getElementById('email')
            const password = document.getElementById("password")
            const btnUpdate = document.getElementById('buttonCriarUsuariosUpdate')
            
            btnUpdate.setAttribute('disabled', 'true')
            fetchall(`${url}/updateuser/`,{
                    nome:nome.value,
                    email:email.value,
                    password: password.value,
                    id:orderId
            },'POST', dataUser.token).then(data=>{
                if(data.message == 'notAuthenticatedUser'){
                    window.localStorage.clear()
                    window.location.assign(url)
                    return
                }
    
                if(data.message == 'success'){
                    window.location.reload()
                }else{
                    document.getElementById('status').textContent = ''
                    btnUpdate.removeAttribute('disabled')
                    btnUpdate.textContent = 'Atualizar dados Usuario'
                }
            })
           
        })
        
    }
    const inputnumber = document.getElementById('numbers')
    const deletes = document.getElementById('delvalues')

    document.querySelectorAll('.finalizarOrders').forEach(e =>{
        let btn = document.getElementById(`order${e.getAttribute('data-id')}`)
        btn.addEventListener('click', async (a)=>{
            let objects = [] || '',valueDue = 0
            let totalValues = document.getElementById('totalValues')
           await fetchall(`${url}/detalhes/`,{
                id:btn.getAttribute('data-id') 
               },'POST',dataUser.token).then( data=>{
                const date = JSON.stringify(data)
                if(date.length > 0){
                    for (const key in data) {
                         let Total = data[key]['produtos']['produto_price'] * data[key]['produto_qtd']
                         valueDue = valueDue + Total
                    }
                }
                
               })
            totalValues.textContent = valueDue
            document.getElementById('prompt').style.display = 'block'
            document.getElementById('pagar').setAttribute('href',`/order/${btn.getAttribute('data-id')}/fine`)
        })
    })
    document.getElementById('sair').addEventListener('click',(a)=> {
        a.preventDefault() 
        inputnumber.value = ''
        document.getElementById('troco').textContent = ''
        document.getElementById('prompt').style.display = 'none'
    })
    
    document.querySelectorAll('.clickBtn').forEach(a=>{
        let button = document.getElementById(a.getAttribute('id'))
        let amount ,Datavalues = document.getElementById('totalValues')
        button.addEventListener('click',(w)=>{
            inputnumber.value = inputnumber.value + button.getAttribute('value')
            amount = parseFloat(Datavalues.textContent)
            if(parseFloat(inputnumber.value) > amount){
                let troco =  (parseFloat(inputnumber.value) - amount)
                alert(troco)
                document.getElementById('troco').textContent = troco
                document.getElementById('pagar').setAttribute('href',`${document.getElementById('pagar').getAttribute('href')}?tl=${troco}`)
            }
        })
    })

    deletes.addEventListener('click',(a)=>{
        document.getElementById('troco').textContent =''
       inputnumber.value = ''
    })
    
     document.onreadystatechange = function (){
         if( document.readyState == "complete" ){
             if(document.getElementById("CheckpageDetalhes")){
            const id = document.querySelectorAll('ul.detalhesOrders')
            
            id.forEach(o=>{

                const detalhes = document.getElementById(o.getAttribute('id'))
                const totalList = o.querySelector(`h6`)
                fetchall(`${url}/detalhes/`,{
                    id:o.getAttribute('name') 
                   },'POST',dataUser.token).then(data=>{
                    if (data.message == 'notAuthenticatedUser') {
                        window.localStorage.clear()
                        window.location.assign(url)
                        return
                    }
                    const date = JSON.stringify(data)
                    const Objects = [] || ''
                    let ValoreDue = 0
                    let idOrder = o.getAttribute('name')
                    if(date.length > 0){
                    for (const key in data) {
                        const div = document.createElement('li')
                        const btn =  document.createElement('button')
                        div.style.padding = '5px'
                        div.className = `${data[key]['id_produto']}-li`
                        div.setAttribute('id',`${key}-li`)
                        div.append(btn)
                        let Total = data[key]['produtos']['produto_price'] * data[key]['produto_qtd']
                        div.textContent = `${data[key]['produtos']['produto_name']} - Quantidade ${data[key]['produto_qtd']} - Valor Total ${Total} akz` 
                        ValoreDue = ValoreDue + Total
                        div.addEventListener('click', ()=>{
                         const li = document.getElementById(`${div.getAttribute('id')}`)
                                    const idP = (div.getAttribute('class')).match(/\d/g).join().replace(/^\d[0/9]/g,'')
                                    li.style.background = 'rgba(212, 2, 2, 0.808)'
                                    li.style.borderRadius = '4px'
                                    li.style.padding = '3px'
                                    if(dataUser.regra == 'Gerente'){
                                        fetchall(`${url}/orderdetalhe/`,{
                                            id: idP,
                                            orderid: idOrder,
                                        },'POST', dataUser.token).then(data=>{
                                           if(data.message == 'success'){
                                            const host = 'http://' + window.location.host + '' + window.location.pathname
                                            window.location.assign(host)
                                           } 

                                           if(data.message == 'notAuthenticatedUser'){
                                               window.localStorage.clear()
                                               const host = 'http://' + window.location.host
                                              window.location.assign(host)
                                           }
                                        })
                                    }  
                                })
                                Objects.push(div)
                             }
                        }
                       
                        for (const key in Objects) {
                           detalhes.append(Objects[key])
                        }
                        totalList.textContent = `Total Devido ${Number(ValoreDue).toFixed(2)} akz`
                   })
            })

         }
        }
     }

  
    function add(a) {
        const [,, orderId,] = (window.location.pathname).split('/')
        const id = a.getAttribute('name')
        a.textContent = 'Adicionando...'
        a.setAttribute('disabled', 'true')
        fetchall(`${url}/order/${orderId}/${id}`, {
            orderid: orderId,
            id: id
        }, 'POST', dataUser.token).then(data=>{
            a.textContent = 'Adicionar Produto'
            a.removeAttribute('disabled')
        })
    }



    if(document.getElementById('formOrderUpdateProdutos')){
        document.getElementById('formOrderUpdateProdutos').addEventListener('submit',(form)=>{
            form.preventDefault()
            const buttonEditarProdutos = document.getElementById('buttonEditarProdutos')
            const produto_name = document.getElementById("produto_name")
            const produto_price = document.getElementById("produto_price")
            const produto_qtd = document.getElementById("produto_qtd")
            const categoriaId = document.getElementById('categoriaId')
            const produto_descrisao = document.getElementById("produto_descrisao")
            buttonEditarProdutos.textContent= 'atualizando...'
            buttonEditarProdutos.setAttribute('disabled','true')
            const [,, id,] = (window.location.pathname).split('/')
            fetchall(`${url}/produtos/update`, {
             produto_name: produto_name.value,
             produto_price: produto_price.value,
             produto_qtd:produto_qtd.value,
             id_categoria: categoriaId.value,
             produto_descrisao: produto_descrisao.value,
             id:id
         }, 'POST', dataUser.token).then((data)=>{
 
 
             if (data.message == 'notAuthenticatedUser') {
                 window.localStorage.clear()
                 window.location.assign(url)
                 
             }
                 if(data.message == 'success'){
                     produto_name.value = ''
                    produto_price.value = '',
                    produto_qtd.value = '',
                    produto_descrisao.value = ''
                     window.location.assign(`${url}/produtos`)
                 }
                 if(data.message){
                     let status = document.getElementById('status')
                     status.textContent = ''
                     status.textContent = data.message  
                     buttonEditarProdutos.textContent= 'Actualizar Produto'
                     buttonEditarProdutos.removeAttribute('disabled')
                     
                 }
         })
        
        })
    }

     if(document.getElementById('formOrderCreateProdutos')){
        const formOrderCreateProdutos = document.querySelector('form[id="formOrderCreateProdutos"]')
        formOrderCreateProdutos.addEventListener('submit',(a)=>{
            a.preventDefault();
            const buttonCriarProdutos = document.getElementById('buttonCriarProdutos')
           const produto_name = document.getElementById("produto_name")
           const produto_price = document.getElementById("produto_price")
           const produto_qtd = document.getElementById("produto_qtd")
           const categoriaId = document.getElementById('categoriaId')
           const produto_descrisao = document.getElementById("produto_descrisao")
           buttonCriarProdutos.textContent= 'Criando...'
           buttonCriarProdutos.setAttribute('disabled','true')
           fetchall(`${url}/produtos/add`, {
            produto_name: produto_name.value,
            produto_price: produto_price.value,
            produto_qtd:produto_qtd.value,
            id_categoria: categoriaId.value,
            produto_descrisao: produto_descrisao.value
        }, 'POST', dataUser.token).then((data)=>{


            if (data.message == 'notAuthenticatedUser') {
                window.localStorage.clear()
                window.location.assign(url)
                
            }
                if(data.message == 'success'){
                    produto_name.value = ''
                   produto_price.value = '',
                   produto_qtd.value = '',
                   produto_descrisao.value = ''
                    window.location.reload()
                }
                if(data.message){
                    let status = document.getElementById('status')
                    status.textContent = ''
                    status.textContent = data.message  
                    buttonCriarProdutos.textContent= 'Criar Produto'
                    buttonCriarProdutos.removeAttribute('disabled')
                    
                }
        })
        })
    }
    if(document.getElementById('formOrderCreate')){
    const formOrderCreates = document.querySelector('form[id="formOrderCreate"]')
    document.getElementById('buttonfecharModel').addEventListener('click',()=>{
        document.getElementById('typeNomelX').value = ''
        document.getElementById('typeEmailX').value = ''
    })
    formOrderCreates.addEventListener('submit', async (a) => {
        a.preventDefault()
        const btnCriar = document.getElementById('buttonCriarOrder')
        const nome = document.getElementById('typeNomelX')
        const email = document.getElementById('typeEmailX')
        const nif = document.getElementById('typeNif')
        btnCriar.textContent = ''
        btnCriar.textContent = 'Criando..'
        btnCriar.setAttribute('disabled', 'true')

        fetchall(`${url}/order`, {
            nome: nome.value,
            email: email.value,
            nif: nif.value
        }, 'POST', dataUser.token).then((data) => {
            if (data.message == 'notAuthenticatedUser') {
                window.localStorage.clear()
                window.location.assign(url)
                return
            }else if(data.message){
                let status = document.getElementById('status')
                status.textContent = ''
                status.textContent = data.message
                btnCriar.textContent = 'Abrir Mesa'
                btnCriar.removeAttribute
                ('disabled')
                return
            }
            window.location.assign(`${url}/order/${
                data.id
            }/add`)

        }).catch(erro => console.log(erro))
    })
}


} else {
    const host = 'http://' + window.location.host + '/'
    window.location.assign(host)
}
