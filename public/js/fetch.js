export async function fetchall(url, data = {}, method, authorization) {
    const response = await fetch(url, {
        method: method, 
        cache: 'no-cache', 
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${authorization}`
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
        body: JSON.stringify(data) 
    });

    return response.json()
}