//import {fetchall} from 'fetch.js'

async function fetchAll(url, data = {}, method, authorization) {
    const response = await fetch(url, {
        method: method,
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${authorization}`
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(data)
    });

    return response.json()
}
const search = document.getElementById('search')
search.addEventListener('keypress', async (a) => {
    const dataUser = JSON.parse(window.localStorage.getItem('User'))
    const input = document.getElementById('search')
    if (search.value != null && search.value != '') {
        const search = await fetchAll(`${url}/produto/search/`, {
            query: input.value
        }, 'POST', dataUser.token)

        if(search.json && search.json.length > 0 ){
            var all = html(search.json)
            $('.produtosist').empty().append(all)
        }
        
    }
})

function html(json){
    var data = [] || ''
    for (const key in json) {
        let construct = ` <div class="col-md" style="min-width: 190px;max-width: 200px;margin: 4px;" >
        <div class="card" style="min-width: 190px;max-width: 200px;margin: 4px;">
                            <div class="card-body">
                                <div class="mb-2">
                                    <h6 class="font-weight-semibold mb-1" style="font-size: small;">
                                        ${json[key].produto_name}
                                    </h6>
                                </div>
                            </div>
                            <div class="card-body bg-light text-center">
                                <div class="card-img-actions">
                                    
                                </div>
                                <h3 class="mb-0 font-weight-semibold" id="price" style="font-size: small;font-weight: bold;"> ${json[key].produto_price} akz</h3>
                                <button type="button" class="btn bg-cart" id="btnClickAdd" style=" padding: 4px; margin-top: 5px; background: rgba(212, 2, 2, 0.808); color: #fff;;border-radius: 10px;border:none;font-family: sans-serif;font-weight: bold;" onclick="add(this)" name="${json[key].id}" style="margin: 10px;"><i class="fa fa-cart-plus mr-2"></i> Adicionar Produto</button>
                            </div>
                        </div>           
   </div> `
        data.push(construct)
    }
    return data
}