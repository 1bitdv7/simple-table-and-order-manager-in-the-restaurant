

  async function fetchall(url, data = {}, method, authorization) {
    const response = await fetch(url, {
        method: method, 
        cache: 'no-cache', 
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${authorization}`
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
        body: JSON.stringify(data) 
    });

    return response.json()
}


async function  deleteOrder(a){
    const idOrder = (a.getAttribute('id')).match(/\d/g).join().replace(/[^0-9]/g,'')
    const order = document.getElementById(`orders${idOrder}`)
    //$(`#${order.getAttribute('id')}`).fadeOut('slow')
    const confirms = prompt('Tens Certeza que desejas eleiminar essa venda !')
    if(confirms == undefined || !confirms){
        alert('por favor digite sim !')
        return
    }
    const dataUser = JSON.parse(window.localStorage.getItem('User'))
    const login = await fetchall(`${url}/deleteorder/${idOrder}`,{},'DELETE',dataUser.token)

    if(login.message == 'success') $(`#${order.getAttribute('id')}`).fadeOut('slow')
    
    
 }