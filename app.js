require('dotenv').config

const express = require('express')

const path = require('path')

const Router = require('./src/api/routes/Routes')

const {handlerError} = require('./src/api/util/util.js')

const cookieParser = require("cookie-parser")

const session = require('express-session')


require('./src/api/database/database')

require('express-async-handler')


const memory = new session()
const app = express()
app.use(cookieParser())
app.use(session({
  secret: `${process.env.secret_session}`,
  resave: true,
  saveUninitialized:true
}))
app.use(express.json())

app.set('view engine','ejs')
app.set('views',path.join(__dirname+'/src/frontend/view'))
app.use(express.static('public'))

app.use('/upload',express.static('upload'))




const port = process.env.PORT || 9000

Router(app)
app.use(handlerError)
app.use(function(req, res, next) {
    console.log('** HTTP Error - 404 for request: ' + req.url);
    res.status(404).redirect("/orders");
  });
app.listen(port,()=> console.log(`Server listen in ${port}`))